<?php

namespace App\Service\Currency;

use App\Exception\WrongCurrencyCodeException;
use App\Model\Currency;

class Extractor {
    /**
     * @param string $data
     * @param string $currencyCode
     * @return Currency
     * @throws WrongCurrencyCodeException
     */
    public function extract(string $data, string $currencyCode): Currency
    {
        $decoded = json_decode($data, true);
        if (isset($decoded['Valute']) && isset($decoded['Valute'][$currencyCode])) {
            $currency = new Currency();
            foreach ($decoded['Valute'][$currencyCode] as $key => $value) {
                $currency->{$key} = $value;
            }
            return $currency;
        }

        $ex = new WrongCurrencyCodeException("currency $currencyCode not found");
        $ex->setCurrencyCode($currencyCode);

        throw $ex;
    }
}