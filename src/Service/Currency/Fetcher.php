<?php

namespace App\Service\Currency;

use App\Model\Currency;

class Fetcher {

    /**
     * @var string
     */
    private $currencyUrl;

    public function __construct(string $currencyUrl)
    {
        $this->currencyUrl = $currencyUrl;
    }

    /**
     * @throws \Exception
     */
    public function getRandom(): Currency {

        $decoded = json_decode(file_get_contents($this->currencyUrl), true);
        if (isset($decoded['Valute'])) {
            $chosenOne = random_int(0, count($decoded['Valute']) - 1);
            $indexedArray = array_values($decoded['Valute']);
            $currency = new Currency();
            foreach ($indexedArray[$chosenOne] as $key => $value) {
                $currency->{$key} = $value;
            }
            return $currency;
        }

        throw new \Exception("не удалось прочитать ответ");

    }
}