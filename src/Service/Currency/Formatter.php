<?php

namespace App\Service\Currency;

class Formatter {
    public function format(float $value): string {
        $valueAsInt = (int) $value;
        $divided = $valueAsInt % 10;
        if ($divided === 1) {
            return 'рублю';
        }
        return 'рублям';
    }
}